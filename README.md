# CalendarRollFIFO

The Calendar FIFO Application is used to cut ply from a roll in FIFO. A ply is cut in an HBC in accordance with the PPC schedule. By using FIFO, you eliminate wastage caused by overaged stock.