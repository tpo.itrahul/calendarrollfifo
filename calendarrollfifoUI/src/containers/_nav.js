export default [
  {
    _name: 'CSidebarNav',
    _children: [
      {
        _name: 'CSidebarNavItem',
        name: 'Calendar Roll FIFO',
        to: '/dashboard',
        icon: 'cil-speedometer',
        badge: {
          color: 'primary',
          text: 'NEW'
        }
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Master',
        route: '/CalendarFIFO',
        icon: 'cil-puzzle',
        items: [
          {
            name: 'PlantMaster',
            to: '/CalendarFIFO/Master/PlantMaster'
          },
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Transactions',
        route: '/CalendarFIFO',
        icon: 'cil-puzzle',
        items: [
          {
            name: 'PlantMaster',
            to: '/CalendarFIFO/Master/PlantMaster'
          },
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Pages',
        route: '/pages',
        icon: 'cil-star',
        items: [
          {
            name: 'Login',
            to: '/pages/login'
          },
          {
            name: 'Register',
            to: '/pages/register'
          },
          {
            name: 'Error 404',
            to: '/pages/404'
          },
          {
            name: 'Error 500',
            to: '/pages/500'
          }
        ]
      }, 
      {
        _name: 'CSidebarNavItem',
        name: 'Calendar Roll FIFO Mob',
        to: '/dashboard',
        icon: 'cil-speedometer',
        badge: {
          color: 'primary',
          text: 'NEW'
        }
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Master',
        route: '/CalendarFIFOMobile',
        icon: 'cil-puzzle',
        items: [
          {
            name: 'CalendarBooking',
            to: '/CalendarFIFOMobile/CalendarBooking'
          },
          {
            name: 'CalendarBooking',
            to: '/CalendarFIFOMobile/CalendarBooking'
          },
        ]
      },
      
      {
        _name: 'CSidebarNavDropdown',
        name: 'Transaction',
        route: '/CalendarFIFOMobile',
        icon: 'cil-puzzle',
        items: [
          {
            name: 'CalendarBooking',
            to: '/CalendarFIFOMobile/CalendarBooking'
          },
          {
            name: 'CalendarBooking',
            to: '/CalendarFIFOMobile/CalendarBooking'
          },
        ]
      },  
                     
    ]
  }
]