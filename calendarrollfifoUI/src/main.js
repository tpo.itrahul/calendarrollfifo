import 'core-js/stable'
import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import { iconsSet as icons } from './assets/icons/icons.js'
import store from './store'
const axios = require("axios");
Vue.config.performance = true
Vue.use(CoreuiVue)
//Vue.prototype.$log = console.log.bind(console)

Vue.prototype.$url = 'https://localhost:44300/'

new Vue({
  el: '#app',
  router,
  store,
  icons,
  template: '<App/>',
  components: {
    App
  },
  data () {
    return {
      authenticated: false,
      mockAccount: {
        username: 'admin',
        password: '1234'
      },
      mockAccountuser: {
        username: 'user',
        password: '1234'
      },
      mockAccountusermob: {
        username: 'mob',
        password: '123'
      }
    }
  },
  mounted () {
    if (!this.authenticated) {
      this.$router.replace({ name: 'Login' })
    }
    else if(!this.authenticated){
      this.$router.replace({ name: 'PlantMaster' })
    }   
  },
  methods: {
    setAuthenticated (status) {
      this.authenticated = status
    },
    logout () {
      this.authenticated = false
    }
  }


})
axios.interceptors.request.use(
  config => {
    return {
      ...config,
      headers: {
        Authorization: "api_key",
        'Access-Control-Allow-Origin': 'http://localhost:8080',
            'Access-Control-Allow-Methods': "GET, POST, OPTIONS, PUT, PATCH, DELETE",
            'Access-Control-Allow-Headers': 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization'
      }
    };
  },
  error => Promise.reject(error)
);
