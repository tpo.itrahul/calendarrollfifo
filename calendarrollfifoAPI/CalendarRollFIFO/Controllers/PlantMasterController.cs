﻿using AutoMapper;
using Entities;
using Entities.DataTransferObjects;
using Entities.Models;
using Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalendarRollFIFO.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PlantMasterController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;



        public PlantMasterController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {

            _logger = logger;
            _repository = repository;
            _mapper = mapper;
            _repository = repository;
        }

        [HttpGet]

        public async Task<IEnumerable<PlantMasterDTO>> GetAllPlants()
        {
            try
            {
                var PlantMaster = await _repository.PlantMaster.GetAllPlants();
                _logger.LogInfo($"Returned all Plant from database.");
                var PlantMasterResult = _mapper.Map<IEnumerable<PlantMasterDTO>>(PlantMaster);
                return PlantMasterResult;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllPlants action: {ex.Message}");
                //return new StatusCode { Ok = false };
                return (IEnumerable<PlantMasterDTO>)StatusCode(500, "Internal server error");
            }

        }


        [HttpPost]
        public IActionResult CreatePlant([FromBody] PlantMasterDTO Plant)
        {
            try
            {
                if (Plant == null)
                {
                    _logger.LogError("Plant object sent from client is null.");
                    return BadRequest("Plant object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Plant object sent from client.");
                    return BadRequest("Invalid model object");
                }


                 //Plant.PlantId = new Guid();
                var PlantEntity = _mapper.Map<PlantMaster>(Plant);




                _repository.PlantMaster.CreatePlant(PlantEntity);
                _repository.Save();

                var createdPlant = _mapper.Map<PlantMasterDTO>(PlantEntity);
                return CreatedAtAction("GetAllPlants", new { id = createdPlant.PlantId }, createdPlant);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreatePlant action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpDelete("{id}")]
        public IActionResult DeletePlant(int id)
        {
            try
            {
                var Plant = _repository.PlantMaster.GetPlantById(id);
                if (Plant == null)
                {
                    _logger.LogError($"Plant with Code: {id}, hasn't been found in db.");
                    return NotFound();
                }
                _repository.PlantMaster.Delete(Plant);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPut("{id}")]
        public IActionResult UpdatePlant(int id, [FromBody] PlantMasterDTO Plant)
        {
            try
            {
                if (Plant == null)
                {
                    _logger.LogError("Plant object sent from client is null.");
                    return BadRequest("Plant object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Plant object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var PlantEntity = _repository.PlantMaster.GetPlantById(id);
                if (PlantEntity == null)
                {
                    _logger.LogError($"Plant with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _mapper.Map(Plant, PlantEntity);
                _repository.PlantMaster.UpdatePlant(PlantEntity);
                _repository.Save();
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
