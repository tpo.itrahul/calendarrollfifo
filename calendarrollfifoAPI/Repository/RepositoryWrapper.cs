﻿using System;
using System.Collections.Generic;
using System.Text;
using Interfaces;
using Entities;
using Microsoft.EntityFrameworkCore;


namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RepositoryContext _repoContext;




        private IPlantMasterRepository _PlantMaster;



        public IPlantMasterRepository PlantMaster
        {
            get
            {
                if (_PlantMaster == null)
                {
                    _PlantMaster = new PlantMasterRepository(_repoContext);
                }
                return _PlantMaster;
            }
        }





        public RepositoryWrapper(RepositoryContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }
        public void Save()
        {


            _repoContext.SaveChanges();
        }
    }
}
