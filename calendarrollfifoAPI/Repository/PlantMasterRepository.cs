﻿using Entities;
using Entities.Models;
using Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repository
{
   public   class PlantMasterRepository : RepositoryBase<PlantMaster>, IPlantMasterRepository
    {

        public PlantMasterRepository(RepositoryContext repositoryContext)
           : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<PlantMaster>> GetAllPlants()


        {

            return await FindAll().ToListAsync();

        }

        public void CreatePlant(PlantMaster plant)

        {

            Create(plant);
        }
        public void DeletePlant(PlantMaster plant)
        {
            Delete(plant);
        }

        public PlantMaster GetPlantById(Int64 plantId)
        {
            return FindByCondition(PlantMaster => PlantMaster.PlantId == plantId).FirstOrDefault();
        }


        public void UpdatePlant(PlantMaster plant)
        {


            Update(plant);
        }

    }
}