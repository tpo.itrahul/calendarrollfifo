﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Repository
{
    /// <summary>
    /// The data row extensions.
    /// </summary>
    public static class DataRowHelper
    {
        /// <summary>
        /// The get parameters for filter.
        /// </summary>
        /// <param name="commandParameters">
        /// The command parameters.
        /// </param>
        /// <param name="primaryKey">
        /// The Primary Key.
        /// </param>
        /// <returns>
        /// The <see cref="Array"/>.
        /// </returns>
        //  internal static SqlParameter[] GetParametersForFilter(List<SqlParameter> commandParameters, string primaryKey)
        // {
        //  var filteredParams = commandParameters.Where(
        //      x => x.ParameterName != "ModifiedDate" && x.ParameterName != "CreatedDate"
        //                               && x.ParameterName != primaryKey
        //                                && x.ParameterName != "Status").Select(x => x).ToArray();

        //  return filteredParams.Select(param => (SqlDbType)param.SqlDbType == SqlDbType.NVarChar ? new SqlParameter(param.ParameterName + "Equals", param.SqlDbType) { Value = param.Value } : param).ToArray();
        // }

        /// <summary>
        /// The get value.
        /// </summary>
        /// <param name="row">
        /// The row.
        /// </param>
        /// <param name="column">
        /// The column.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        internal static object GetValue(this DataRow row, string column)
        {
            // try
            //{
            return row.Table.Columns.Contains(column) ? row[column] : null;
            //}
            // catch (Exception e)
            // {
            //     var exception = new ExceptionLog();
            //    var result = exception.Save(
            //        new ExceptionLogModel()
            //       {
            //             Message = e.Message,
            //               CreatedBy = 1
            //             });
            //      return null;
            // }
        }

        /// <summary>
        /// The get value.
        /// </summary>
        /// <param name="row">
        /// The row.
        /// </param>
        /// <param name="column">
        /// The column.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        internal static object GetValue(this DataRow row, int column)
        {
            /// try
            // {
            return row[column];
            // }
            // catch (Exception e)
            //  {
            //     var exception = new ExceptionLog();
            //     var result = exception.Save(
            //        new ExceptionLogModel()
            //          {
            //             Message = e.Message,
            //               CreatedBy = 1
            //           });
            //    return 0;
        
    
        }
    }
}