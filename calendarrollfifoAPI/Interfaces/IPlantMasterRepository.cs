﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IPlantMasterRepository : IRepositoryBase<PlantMaster>
    {

        Task<IEnumerable<PlantMaster>> GetAllPlants();



        void CreatePlant(PlantMaster plant);


        void DeletePlant(PlantMaster plant);

        PlantMaster GetPlantById(Int64 plantId);


        void UpdatePlant(PlantMaster plant);
    }
}