﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    public interface IRepositoryWrapper
    {
        IPlantMasterRepository PlantMaster { get; }

        void Save();
    }
}
