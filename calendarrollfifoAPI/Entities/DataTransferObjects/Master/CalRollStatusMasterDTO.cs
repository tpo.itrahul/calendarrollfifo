﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DataTransferObjects
{
    public class CalRollStatusMasterDTO
    {
        public int CalRollStatusMasterId { get; set; }
        public string CalRollStatusName { get; set; }
        public Boolean CalRollStatus { get; set; }
    }
}
