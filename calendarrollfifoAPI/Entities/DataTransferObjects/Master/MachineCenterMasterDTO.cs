﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DataTransferObjects
{
    public class MachineCenterMasterDTO
    {

        public int MachineCenterId { get; set; }


        public string MachineCenterNo { get; set; }


        public string MachineCenterName { get; set; }


        public Boolean MachineCenterStatus { get; set; }


    }
}
