﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DataTransferObjects
{
    public class UserDTO
    {
        public int UserId { get; set; }

        public int EmployeeId { get; set; }
        public string LoginId { get; set; }
        public string Password { get; set; }
        public Boolean UserStaus { get; set; }

    }
}
