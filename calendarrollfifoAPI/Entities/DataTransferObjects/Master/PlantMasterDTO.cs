﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DataTransferObjects
{
    public class PlantMasterDTO
    {

        public int PlantId { get; set; }

        [Required]

        public int PlantCode { get; set; }
        [Required]
        [MinLength(4), MaxLength(10)]
        public string PlantName { get; set; }

        [Required]
        [MinLength(4), MaxLength(50)]
        public string PlantAddress { get; set; }

        public Boolean PlantStaus { get; set; }


    }
}
