﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DataTransferObjects
{
    public class CalRollTypeMasterDTO
    {

        public int CalRollTypeMasterId { get; set; }

        public string CalRollTypeName { get; set; }


        public Boolean CalRollTypeStaus { get; set; }


    }
}
