﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DataTransferObjects
{
    public class ScheduleMasterDTO
    {

        public int ScheduleId { get; set; }


        public int ScheduleOrder { get; set; }


        public Boolean ScheduleStaus { get; set; }

    }
}
