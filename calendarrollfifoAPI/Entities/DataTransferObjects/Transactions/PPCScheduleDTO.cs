﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DataTransferObjects
{
  public class PPCScheduleDTO
    {
        public string ItemCode { get; set; }

        public int Priority { get; set; }
        public Boolean ScheduledQuantity { get; set; }
        public int SequenceNo { get; set; }
        public string Remarks { get; set; }
        public string Date { get; set; }
        public int shift { get; set; }
    }
}
