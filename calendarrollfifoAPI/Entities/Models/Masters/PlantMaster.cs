﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    [Table("PlantMaster")]
    public class PlantMaster
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PlantId { get; set; }
        [Required(ErrorMessage = "Plant_Code is required")]

        public int PlantCode { get; set; }
        [Required(ErrorMessage = "Plant code is required")]
        [StringLength(100, ErrorMessage = "Plant code can't be longer than 100 characters")]

        public string PlantName { get; set; }
        [Required(ErrorMessage = "Plant Name is required")]
        [StringLength(100, ErrorMessage = "Plant Name can't be longer than 100 characters")]

        public string PlantAddress { get; set; }
        [Required(ErrorMessage = "Address is required")]
        [StringLength(500, ErrorMessage = "Address can't be longer than 500 characters")]

        public Boolean PlantStaus { get; set; }


    }
}
