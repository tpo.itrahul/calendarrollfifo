﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    [Table("CalRollTypeMaster")]
    public class CalRollTypeMaster
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CalRollTypeMasterId { get; set; }
        [Required(ErrorMessage = "CalRoll Type Master Id is required")]

        public string CalRollTypeName{ get; set; }
        [Required(ErrorMessage = "CalRollType Name is required")]
        [StringLength(100, ErrorMessage = "CalRollType Name can't be longer than 100 characters")]

        public Boolean CalRollTypeStaus { get; set; }

    }
}
