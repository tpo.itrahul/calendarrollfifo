﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    [Table("MachineCenterMaster")]
    public  class MachineCenterMaster
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MachineCenterId { get; set; }
        [Required(ErrorMessage = "Machine Center Id is required")]

        public string MachineCenterNo { get; set; }

        [Required(ErrorMessage = "Machine Center Number is required")]
        [StringLength(100, ErrorMessage = "Machine Center Number can't be longer than 100 characters")]
        public string MachineCenterName { get; set; }

        [Required(ErrorMessage = "Machine Center Name is required")]
        [StringLength(100, ErrorMessage = "Machine Center Name can't be longer than 100 characters")]

        public Boolean MachineCenterStatus { get; set; }
    }
}
