﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    [Table("CalRollStatusMaster")]
    public class CalRollStatusMaster
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CalRollStatusMasterId { get; set; }
        [Required(ErrorMessage = "Calendar Roll Status Master Id is required")]

        public string CalRollStatusName { get; set; }

        [Required(ErrorMessage = "Calendar Roll Status  Name is required")]
        [StringLength(100, ErrorMessage = "Calendar Roll Status  Name can't be longer than 100 characters")]
        public Boolean CalRollStatus { get; set; }
    }
}
