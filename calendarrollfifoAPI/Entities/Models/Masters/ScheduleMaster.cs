﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    [Table("ScheduleMaster")]
   public class ScheduleMaster
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ScheduleId { get; set; }
        [Required(ErrorMessage = "Schedule Id is required")]

        public int ScheduleOrder { get; set; }

        [Required(ErrorMessage = "Schedule Order  is required")]

        public Boolean ScheduleStaus { get; set; }
    }
}
